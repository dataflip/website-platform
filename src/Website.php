<?php

namespace DataBackFlip;

use Exception;
use Throwable;

class Website
{
    private string $tablePrefix = 'zzp_';
    private string $configTablePrefix = 'zzz_';
    private Data $conn;
    private Data $systemConn;
    public array $siteObject;
    public array $server;
    public array $get;
    public array $post;
    public array $thisPage;
    public array $extensionExists;
    public array $pageBreadCrumbs;
    public mixed $request;
    const expectedDbKeys = [
        "host",
        "username",
        "password",
        "database"
    ];

    /**
     * constructor class
     * @param array $currentDBConfig - current database configuration and must have keys in @param const self::expectedDbKeys
     * @param array $systemDBConfig - system database configuration and must have keys in @param const self::expectedDbKeys
     * @param string|null $tablePrefix - database table prefix examples;
     * @param string|null $configTablePrefix
     * @throws Exception
     * @todo find a way to get $this->config
     */
    public function __construct(array $currentDBConfig, array $systemDBConfig, string $tablePrefix = NULL, string $configTablePrefix = NULL)
    {
        $this->tablePrefix = $tablePrefix ?? $this->tablePrefix;
        $this->configTablePrefix = $configTablePrefix ?? $this->configTablePrefix;
        $this->conn = new Data("current", $currentDBConfig);
        $this->systemConn = new Data("system", $systemDBConfig);
        $this->server = $_SERVER;
        $this->get = $_GET;
        $this->post = $_POST;
        $this->request = json_decode(file_get_contents("php://input"), true);
    }

    /**
     * Return details of the page being served
     * // check if $totalUrls == 1
     * // --> url is not null, $this->configTablePrefix.table_id is null, relationship_table_id is null, parent_id is null
     * // check if $totalUrls == 2
     * // --> either static (url is not null, parent_id = level_1_id, $this->configTablePrefix.table_id is null, relationship_table_id is null)
     * // --> or dynamic (url is null, parent_id = level_1_id, $this->configTablePrefix.table_id && $this->configTablePrefix.field_id are not null, relationship_table_id is null)
     * // check if totalUrls > 2
     * // --> All routes that have $this->configTablePrefix.table_id as not null must have a static parent and $this->configTablePrefix.field_id should not be null
     * // --> All routes that have  $this->configTablePrefix.relationship_table_id as not null must have dynamic parent or parent.relationship_table_id not null
     * // --> All routes that have url as not null must have static parent
     * @return array
     * @throws Throwable
     */
    public function run()
    {
        try {
            $urls = $this->getPageUrls();
            $this->thisPage = [];
            $this->pageBreadCrumbs = [];
            $totalExtensions = count($this->extensionExists);
            if (!is_null($urls) && $totalExtensions == 0) {
                $totalUrls = count($urls);
                if ($totalUrls < 2) {
                    if ($totalUrls == 0) {
                        $totalUrls = 1;
                        $urls = ["/"];
                    }

                    $query = $this->getFirstPageQuery();
                } else {
                    $query = $this->getPagesQuery(str_repeat("?,", $totalUrls - 2) . "?", $totalUrls);
                }

                $pages = $this->conn->getRows($query, $urls);
                $processedLevel = [];
                if (is_array($pages) && count($pages) >= $totalUrls) {
                    $parentFieldId = 0;
                    $parentPivotColumn = "";
                    $fieldTypes = $this->systemConn->getAllRows('field_types') ?? [];
                    $FieldTypeNames = array_column($fieldTypes, 'type_name', 'id');
                    foreach ($pages as $k => $page) {
                        $subQuery = $page["sub_query"];
                        unset($pages[$k]["sub_query"]);
                        if (empty($subQuery)) {
                            $contents = [];
                            if (!empty($page["content"])) {
                                $page["content"] = json_decode($page["content"], true);
                                foreach ($page["content"] as $j => $content) {
                                    $content['content_type'] = $FieldTypeNames[$content['content_type_id']];
                                    if (!empty($content['content_type'])) {
                                        if ($content["content_type"] == "text") {
                                            $content["content"] = htmlspecialchars($content["content"]);
                                        } else if ($content["content_type"] == "textarea") {
                                            $content["content"] = nl2br(htmlspecialchars($content["content"]));
                                        }

                                        $contents[] = $content;
                                    }
                                }
                            }

                            $pages[$k]["content"] = $contents;
                            $processedLevel[$page['id']] = $page['level'];
                        } else {
                            $this->conn->getRows($subQuery);
                            if (count($this->conn->row) == 0 || !isset($processedLevel[$page['parent_id']]) || (end($processedLevel) > $page['level'] && count($pages) < $page['level'] - 1)) {
                                $pages = [];
                                break;
                            } else {
                                $parentRow = $this->conn->row;
                                if (isset($this->conn->row["use_pivot_table"])) {
                                    if ($this->conn->row["use_pivot_table"]) {
                                        $this->conn->getRows("SELECT {$this->conn->row["table_name"]}.* FROM {$this->conn->row["table_name"]} 
                                        INNER JOIN {$this->conn->row["pivot_table"]} ON {$this->conn->row["table_name"]}.id={$this->conn->row["pivot_table"]}.{$this->conn->row["pivot_column_name"]}
                                        WHERE {$this->conn->row["pivot_table"]}.$parentPivotColumn=? AND {$this->conn->row["table_name"]}.{$this->conn->row["field_name"]}=?", [$parentFieldId, $urls[$page['level'] - 1]]);
                                    } else {
                                        $this->conn->getRows("SELECT * FROM {$this->conn->row["table_name"]} WHERE $parentPivotColumn=? AND {$this->conn->row["field_name"]}=?", [$parentFieldId, $urls[$page['level'] - 1]]);
                                    }
                                } else {
                                    $this->conn->getRows("SELECT * FROM {$this->conn->row["table_name"]} WHERE {$this->conn->row["field_name"]}=?", [$urls[$page['level'] - 1]]);
                                }

                                if (count($this->conn->row) == 0) {
                                    if (in_array($page['level'], $processedLevel) || (count(array_filter($pages, function ($item) use ($page) {
                                                return $item['level'] == $page['level'];
                                            })) > 1)) {
                                        unset($pages[$k]);
                                    } else {
                                        $pages = [];
                                        break;
                                    }
                                } else {
                                    $parentPivotColumn = $parentRow["pivot_column_name"];
                                    $parentFieldId = $this->conn->row["id"];
                                    $pages[$k]["content"] = [$this->conn->row];
                                    $processedLevel[$page['id']] = $page['level'];
                                }
                            }
                        }
                    }

                    $totalPages = count($pages);
                    if ($totalPages >= $totalUrls) {
                        $this->thisPage = $totalPages > 0 ? end($pages) : [];
                        $this->pageBreadCrumbs = $pages;
                    }
                }

                return $this->thisPage;
            }
        } catch (Throwable $th) {
            throw $th;
        }
    }

    /**
     * get url segments/splits
     * @return array|null
     */
    private function getPageUrls(): array|null
    {
        $urls = null;
        if (isset($this->server["REQUEST_URI"]) && !empty($this->server["REQUEST_URI"])) {
            $url = $this->server["REQUEST_URI"];
            $path_parts = [];
            $this->extensionExists = [];
            if (pathinfo($url) && parse_url($url)) {
                $path_parts = pathinfo($url) + parse_url($url);
            } else if (pathinfo($url)) {
                $path_parts = pathinfo($url);
            } else if (parse_url($url)) {
                $path_parts = parse_url($url);
            }

            if (isset($path_parts["query"])) {
                $url = str_replace("?" . $path_parts["query"], "", $url);
            }

            $urls = explode("/", $url);
            if (isset($path_parts["extension"])) {
                $this->extensionExists = (array)array_pop($urls) ?? [];
            }

            $urls = array_values(array_filter($urls));
        }

        return $urls;
    }

    /**
     * Dynamic query for more than 1  url splits
     */
    private function getPagesQuery(string $urlStr, int $recursiveCount): string
    {
        $firstPageQuery = $this->getFirstPageQuery();
        $lastRecursion = $recursiveCount - 1;
        return "WITH RECURSIVE pages_cte AS (
            $firstPageQuery
            UNION ALL
            SELECT child.id,child.url,pages_cte.level+1 as level, pages_cte.id AS parent_id,child.{$this->tablePrefix}website_route_id AS website_route_id,child.{$this->configTablePrefix}table_id AS table_id,child.{$this->configTablePrefix}relationship_table_id AS relationship_table_id,child.{$this->configTablePrefix}field_id AS field_id,
                website_components.id AS component_id,website_components.name AS component_name, website_components.class AS component_class,
                IF(
                    child.url IS NULL OR REPLACE(child.url, ' ', '') ='',JSON_ARRAY(),
                    COALESCE((SELECT JSON_ARRAYAGG(JSON_OBJECT('content_id',website_contents.id,'content',website_contents.content,'content_type_id',website_variables.field_type_id,'variable_id', website_variables.id, 'variable_name',website_variables.name))  AS content
                    FROM {$this->tablePrefix}website_contents website_contents
                    INNER JOIN {$this->tablePrefix}website_variables website_variables ON website_contents.{$this->tablePrefix}website_variable_id = website_variables.id
                    WHERE child.id = website_contents.{$this->tablePrefix}website_route_id AND website_components.id = website_variables.{$this->tablePrefix}website_component_id
                    GROUP BY website_contents.{$this->tablePrefix}website_route_id,website_variables.{$this->tablePrefix}website_component_id),JSON_ARRAY())
                ) AS content,
                IF(child.{$this->configTablePrefix}table_id IS NOT NULL, CONCAT('SELECT {$this->configTablePrefix}tables.table_name,{$this->configTablePrefix}fields.field_name,{$this->configTablePrefix}tables.pivot_column_name FROM {$this->configTablePrefix}tables INNER JOIN {$this->configTablePrefix}fields ON {$this->configTablePrefix}tables.id = {$this->configTablePrefix}fields.{$this->configTablePrefix}table_id WHERE {$this->configTablePrefix}tables.id=',child.{$this->configTablePrefix}table_id,' AND {$this->configTablePrefix}fields.id=',child.{$this->configTablePrefix}field_id),
                IF(child.{$this->configTablePrefix}relationship_table_id IS NOT NULL, CONCAT('SELECT {$this->configTablePrefix}tables.table_name,{$this->configTablePrefix}fields.field_name,{$this->configTablePrefix}tables.pivot_column_name,{$this->configTablePrefix}relationships.use_pivot_table,IF({$this->configTablePrefix}relationships.use_pivot_table,(SELECT table_name FROM {$this->configTablePrefix}tables WHERE id={$this->configTablePrefix}relationships.pivot_table_id),NULL) AS pivot_table FROM {$this->configTablePrefix}relationship_tables INNER JOIN {$this->configTablePrefix}relationships ON {$this->configTablePrefix}relationship_tables.{$this->configTablePrefix}relationship_id={$this->configTablePrefix}relationships.id AND {$this->configTablePrefix}relationships.relationship_type_id IN(2,3)  INNER JOIN {$this->configTablePrefix}tables ON {$this->configTablePrefix}relationship_tables.{$this->configTablePrefix}table_id={$this->configTablePrefix}tables.id INNER JOIN {$this->configTablePrefix}fields ON {$this->configTablePrefix}tables.id={$this->configTablePrefix}fields.{$this->configTablePrefix}table_id WHERE {$this->configTablePrefix}relationship_tables.id=',child.{$this->configTablePrefix}relationship_table_id,' AND {$this->configTablePrefix}fields.id=',child.{$this->configTablePrefix}field_id),NULL)) AS sub_query
            FROM {$this->tablePrefix}website_routes child
            INNER JOIN pages_cte ON (IF(child.{$this->tablePrefix}website_route_id = pages_cte.id AND pages_cte.level = $lastRecursion,child.{$this->tablePrefix}website_route_id = pages_cte.id,IF((pages_cte.relationship_table_id IS NOT NULL AND (SELECT referenced_table = {$this->configTablePrefix}table_id FROM {$this->configTablePrefix}relationship_tables WHERE {$this->configTablePrefix}relationship_tables.id = pages_cte.relationship_table_id) = true), child.id = pages_cte.id,child.{$this->tablePrefix}website_route_id = pages_cte.id)))
            LEFT JOIN {$this->tablePrefix}website_components website_components ON child.{$this->tablePrefix}website_component_id = website_components.id
            WHERE pages_cte.level < $recursiveCount AND IF(child.url IS NOT NULL AND REPLACE(child.url, ' ', '') <>'',child.url IN ({$urlStr}) AND child.{$this->configTablePrefix}table_id IS NULL AND child.{$this->configTablePrefix}relationship_table_id IS NULL AND IF(pages_cte.level=1,1, pages_cte.url IS NOT NULL AND REPLACE(pages_cte.url, ' ', '') <>'' AND pages_cte.relationship_table_id IS NULL AND pages_cte.table_id IS NULL),
            child.{$this->configTablePrefix}field_id IS NOT NULL AND IF(child.{$this->configTablePrefix}table_id IS NOT NULL, child.{$this->configTablePrefix}field_id=(SELECT id FROM {$this->configTablePrefix}fields WHERE {$this->configTablePrefix}table_id=child.{$this->configTablePrefix}table_id AND id=child.{$this->configTablePrefix}field_id) AND child.{$this->configTablePrefix}relationship_table_id IS NULL AND IF(pages_cte.level=1,1, pages_cte.url IS NOT NULL AND pages_cte.relationship_table_id IS NULL AND pages_cte.table_id IS NULL),
            child.{$this->configTablePrefix}relationship_table_id IS NOT NULL AND (pages_cte.url IS NULL OR REPLACE(pages_cte.url, ' ', '') ='') AND IF(pages_cte.table_id IS NOT NULL, pages_cte.relationship_table_id IS NULL AND pages_cte.table_id=(SELECT referenced_table FROM {$this->configTablePrefix}relationship_tables WHERE {$this->configTablePrefix}relationship_tables.id=child.{$this->configTablePrefix}relationship_table_id),pages_cte.relationship_table_id IS NOT NULL AND pages_cte.relationship_table_id=(SELECT p.id FROM {$this->configTablePrefix}relationship_tables p INNER JOIN {$this->configTablePrefix}relationship_tables c ON p.{$this->configTablePrefix}table_id=c.referenced_table WHERE c.id=child.{$this->configTablePrefix}relationship_table_id AND p.id=pages_cte.relationship_table_id))))
        )
        SELECT pages_cte.* FROM pages_cte ORDER BY pages_cte.level";
    }

    /**
     * Query for first url split
     */
    private function getFirstPageQuery(): string
    {
        return "SELECT parent.id,parent.url,1 as level,CAST(NULL AS CHAR(10000)) AS parent_id, parent.{$this->tablePrefix}website_route_id AS website_route_id,parent.{$this->configTablePrefix}table_id AS table_id,parent.{$this->configTablePrefix}relationship_table_id AS relationship_table_id,parent.{$this->configTablePrefix}field_id AS field_id,
            website_components.id AS component_id,website_components.name AS component_name, website_components.class AS component_class,
            (
                SELECT JSON_ARRAYAGG(JSON_OBJECT('content_id',website_contents.id,'content',website_contents.content,'content_type_id',website_variables.field_type_id,'variable_id', website_variables.id, 'variable_name',website_variables.name))  AS content
                FROM {$this->tablePrefix}website_contents website_contents
                INNER JOIN {$this->tablePrefix}website_variables website_variables ON website_contents.{$this->tablePrefix}website_variable_id = website_variables.id
                WHERE parent.id = website_contents.{$this->tablePrefix}website_route_id AND website_components.id = website_variables.{$this->tablePrefix}website_component_id
                GROUP BY website_contents.{$this->tablePrefix}website_route_id,website_variables.{$this->tablePrefix}website_component_id
            ) AS content,
            CAST(NULL AS CHAR(10000))AS sub_query
        FROM {$this->tablePrefix}website_routes parent
        LEFT JOIN {$this->tablePrefix}website_components website_components ON parent.{$this->tablePrefix}website_component_id = website_components.id
        WHERE parent.url=? AND parent.{$this->configTablePrefix}relationship_table_id IS NULL AND parent.{$this->configTablePrefix}table_id IS NULL AND parent.{$this->tablePrefix}website_route_id IS NULL";
    }
}