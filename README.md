Databackflip Website
==========
## Pre-Installation

- Get access to this library from the owner since this is a private library
- The ownwer should run the Website plugin configurations to set the config tables to your database

## Installation

Installation is super-easy via [Composer](https://getcomposer.org/):

```bash
$ composer require databackflip/website
```

or add it by hand to your `composer.json` file.

Installation will fail if you don't have access to this library

## Usage
Set Database Configuration Array, Create an instance of `\Databackflip\Website`, and run it.

```php
// Require composer autoloader
require __DIR__ . '/vendor/autoload.php';

//Set Database Configuration Array
$db = [
    "host" => "localhost",
    "username" => "root",
    "password" => "password",
    "database" => "test"
];
// Create Website instance
$site = new DataBackFlip\Website($db);

// Run it!
$site->run();
```

### Routing

Enter a URL, when the URL Path matches stored values, a page is served, else a 404 Error page is returned 