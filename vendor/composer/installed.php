<?php return array(
    'root' => array(
        'name' => 'databackflip/website',
        'pretty_version' => 'dev-main',
        'version' => 'dev-main',
        'reference' => '0a7d2a3b4176438cb3d91cdeed01bfcaa379ba48',
        'type' => 'library',
        'install_path' => __DIR__ . '/../../',
        'aliases' => array(),
        'dev' => true,
    ),
    'versions' => array(
        'databackflip/data' => array(
            'pretty_version' => 'dev-main',
            'version' => 'dev-main',
            'reference' => '09ba13eddee920367f9ed7eb20b1b848691de649',
            'type' => 'library',
            'install_path' => __DIR__ . '/../databackflip/data',
            'aliases' => array(
                0 => '9999999-dev',
            ),
            'dev_requirement' => false,
        ),
        'databackflip/website' => array(
            'pretty_version' => 'dev-main',
            'version' => 'dev-main',
            'reference' => '0a7d2a3b4176438cb3d91cdeed01bfcaa379ba48',
            'type' => 'library',
            'install_path' => __DIR__ . '/../../',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
    ),
);
